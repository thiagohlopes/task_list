const mysql = require('mysql');

const pool = mysql.createPool({
    connectionLimit: 10,
    password: '12345678',
    user: 'root',
    database: 'mydb',
    host: 'localhost',
    port: '3306'
});

let mytaskdb = {};

mytaskdb.fetchAllTask = () => {

    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM task`, (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });

};

// mytaskdb.findProductById = (id) => {

//     return new Promise((resolve, reject) => {
//         pool.query(`SELECT * FROM products WHERE product_id = ?`, [id], (err, results) => {
//             if (err) {
//                 return reject(err);
//             }
//             return resolve(results[0]);
//         })
//     });

// };

// mytaskdb.findProductByName = (name) => {
//     name = '%' + name + '%';

//     return new Promise((resolve, reject) => {
//         pool.query(`SELECT * FROM products WHERE product_name LIKE ?`, [name], (err, results) => {
//             if (err) {
//                 return reject(err);
//             }
//             return resolve(results);
//         })
//     });

// };

mytaskdb.newTask = (task) => {

    return new Promise((resolve, reject) => {
        let created_at = new Date();
        pool.query(`INSERT INTO task(title, description, Data) VALUES (?,?,?)`, [task.title, task.description, created_at], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });

};

mytaskdb.updateTask = (id, task) => {

    return new Promise((resolve, reject) => {
        pool.query(`UPDATE task SET title = ?, description = ? WHERE id = ?`, [task.title, task.description, id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });

};

mytaskdb.removeTask = (id) => {

    return new Promise((resolve, reject) => {
        pool.query(`DELETE FROM task WHERE id = ?`, [id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        })
    });
}

module.exports = mytaskdb;