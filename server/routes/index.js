const express = require('express');
const db = require('../db');
const router = express.Router();

router.get('/task/', async (req, res, next) => {
    try {
        let alltask = await db.fetchAllTask();
        res.json(alltask);
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.post('/task/', async (req, res, next) => {
    try {
        let newtask =  await db.newTask(req.body);
        res.json(newtask);
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.put('/task/:id', async (req, res, next) => {
    try {
        let task = await db.updateTask(req.params.product_id, req.body);
        if (task === undefined) { res.sendStatus(404); }
        res.sendStatus(200);
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.delete('/task/:id', async (req, res, next) => {
    try {
        await db.removeTask(req.params.id);
        res.sendStatus(200);
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
})
module.exports = router;